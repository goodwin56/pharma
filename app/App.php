<?php

namespace app;

class App {
    
    private $_search = [
        "4008500130407",
        "4603933000725",
        "8002660019837",
        "4607009582078"
    ];
    
    private $_outCsv = [];
    
    public function start() {
        foreach($this->_search as $search){
            $parser = parser\Zhivika::run($search);
            $this->_outCsv[] = [
                $search,
                $parser->name,
                $parser->price,
            ];           
        }
        $this->save();
    }

    private function save(){

        $fp = fopen('app/out/file.csv', 'w');

        foreach ($this->_outCsv as $fields) {
            fputcsv($fp, $fields);
        }

        fclose($fp);        
    }

    public static function run() {
        return (new self())->start();
    }

}
