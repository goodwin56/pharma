<?php

namespace app\parser;

use phpQuery;

abstract class ParserInterface {

    public $searchCode = "";
    public $price = "";
    public $name = "";

    /**
     *
     * @var phpQueryObject
     */
    public $_htmlBody;
    private $_errors = [];

    abstract public function setPrice();

    abstract public function setName();

    abstract public function getSearchUrl();
    
    private function getUserAgent(){
        return 'Mozilla/5.0 (Windows 8) AppleWebKit/534.30 (KHTML, like Gecko) Chrome/12.0.742.112 Safari/534.30';
    }

    private function addError($e) {
        $this->_errors[] = $e;
        return true;
    }

    public function getErrors() {
        return $this->_errors;
    }

    public function getPageHtml() {
        echo "start parse " . $this->getSearchUrl() . "\n";

        $client = new \GuzzleHttp\Client(['headers' => ['User-Agent' => $this->getUserAgent()]]);
        $res = $client->request('GET', $this->getSearchUrl());
        
        $body = $res->getBody();
        
        $this->_htmlBody = phpQuery::newDocument($body);
    }

}
