<?php

namespace app\parser;

use app\parser\ParserInterface;

class Zhivika extends ParserInterface {

    private function __construct($searchCode) {
        $this->searchCode = $searchCode;
    }

    public function getSearchUrl() {
        return "http://chel.zhivika.ru/plugins/catalog/catalog_search?searchText=".$this->searchCode."&pharm=&sortsearch=0&x=0&y=0";
    }

    public function setName() {
        $this->name = pq($this->_htmlBody)->find('.one_line_name')->eq(0)->text();
    }

    public function setPrice() {
        $this->price = pq($this->_htmlBody)->find('.booking')->eq(0)->text();
    }

    public function parse() {
        $this->getPageHtml();
        $this->setName();
        $this->setPrice();
        return $this;
    }

    public static function run($searchCode) {
        return (new self($searchCode))->parse();
    }

}
